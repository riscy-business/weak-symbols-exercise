// SPDX-License-Identifier: (MIT OR Unlicense)
#include <stdio.h>
#include <stdlib.h>
#include "foo.h"
int main(int argc, char **argv)
{
  fprintf(stdout, "%s\n", foo());
  return 0;
}
