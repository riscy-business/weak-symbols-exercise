// SPDX-License-Identifier: (MIT OR Unlicense)
#pragma once
#pragma weak foo
/* __attribute__((weak)) */
char * foo(void);
