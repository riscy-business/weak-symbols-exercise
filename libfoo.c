// SPDX-License-Identifier: (MIT OR Unlicense)
#include "foo.h"
char * foo(void)
{
    return "foo.c: Hello, World!";
}
